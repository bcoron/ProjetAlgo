#!/usr/bin/env python
# -*- coding: utf-8 -*-

def equivalence_voisinage(n, v1, v2, bijection):
    if len(v1) != len(v2):
        return False

    for j in v1:
        #test limité aux n premiers sommets de G1
        if (j<n) and not(bijection[j] in v2):
            return False
    return True

# La bijection est un tableau t tel que t[i] = p(i) où p est une bijection.
# G1, G2 sont des graphes définis par liste d'adjacence.
# n : nombre de somets
def tester_isomorphisme(n, G1, G2, bijection):
    for i in range(n):
        voisins_p_i = G2[bijection[i]] # voisins de p(i) dans G2.
        voisins_i   = G1[i]
        if not(equivalence_voisinage(n, voisins_i, voisins_p_i, bijection)):
            return False
    return True

def partition_degre(n, G):
    p = [[] for _ in range(n)]
    for i in range(n):
        p[len(G[i])].append(i)
    return p

def compatible(x, y, p, G1, G2):
    voisins_x = G1[x]
    voisins_y = G2[y]
    #TODO: Trouver une bonne structure de données pour le partiionnement.

def partitionnement_stable(p, G1, G2):
    np = []

    for lst in p:
        partition_locale = []
        traites = []
        for x in lst:
            if not(x in traites):
                partition_locale.append([x])
                traites.append(x)
                for y in lst:
                    if not(y in traites):
                        if compatible(x, y, p, G1, G2):
                            partition_locale[-1].append(y)
                            traites.append(y)
        np += partition_locale
    return np

def choisir_image(n, G1, G2, position, choisi, bijection, raffinement):
    degre_pos = len(G1[position])

    for test in range(bijection[position]+1,n):
        if (test in raffinement) and not(choisi[test]):
            voisins_position = G1[position]
            voisins_test     = G2[test]
            bijection[position] = test

            if equivalence_voisinage(position+1, voisins_position,
                                     voisins_test, bijection):
                return test
    return -1

def backtracking_glouton(n, G1, G2, raffinement=0):
    bijection = [-1]*n
    choisi    = [False]*n
    position  = 0
    degres_2  = partition_degre(n, G2)

    while(position < n):
        # on doit déterminer l'image de v_position.
        restriction = [i for i in range(n)] if raffinement == 0 else degres_2[len(G1[position])]
        #print("position:"+str(position)+" -> "+str(restriction))
        choix = choisir_image(n, G1, G2, position, choisi, bijection, restriction)

        if choix != -1:
            # on avance.
            choisi[choix] = True
            bijection[position] = choix
            position += 1
        else:
            # on recule.
            bijection[position] = -1
            position -= 1
            choisi[bijection[position]] = False
            if position == 0 and bijection[position] == n-1:
                return False
    return bijection
