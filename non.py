#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
f = codecs.open("main0.py","r","utf-8")
fout = codecs.open("main1.py","w","utf-8")


def getMagic(buf):
    keywords = ["and","else","if","True","False","while","return","for","def","print","input","in","range","python","utf","coding","!/usr/bin/env","not"]
    [keywords.append(str(n)) for n in range(10)]

    for k in keywords:
        if buf == k:
            return buf

    return "".join([chr(ord(c)+4352) for c in list(buf)])


c = '0'
buf = u''
while(c != ""):
    c = f.read(1)
    if c in [':','!','\n','+','>','<','_',' ','(',')',',','[',']','.','=','*','#','\t','-']:
        fout.write(getMagic(buf))
        fout.write(c)
        buf = u''
    else:
        buf = buf+c
fout.close()
