# ==============================================================================
# ==================== FONCTIONS UTILITAIRES ===================================
# ==============================================================================
import random

def ecrire_graphe(G, fichier):
    f = open(fichier, "w")
    n, m = len(G), 0
    f.write(str(n) + " " + str(m) + "\n")
    for l in G:
        s = len(l)
        f.write(str(s))
        for x in l:
            f.write(" " + str(x))
        f.write("\n")
    f.close()

# Algorithme de Fisher-Yates
def permutation_aleatoire(n):
    perm = [i for i in range(n)]
    for i in reversed(range(1,n)):
        j = random.randint(0,i)
        perm[i], perm[j] = perm[j], perm[i]
    return perm

# Génération de type Erdös-Renyi
def generer_paire_graphes(n, p, force_isomorphism):
    def erdos_renyi():
        G = [[] for _ in range(n)]
        for i in range(n):
            for j in range(i):
                if random.random() <= p:
                    G[i].append(j)
                    G[j].append(i)
        return G
    G1 = erdos_renyi()
    if force_isomorphism:
        perm = permutation_aleatoire(n)
        G2 = [[] for _ in range(n)]
        for i in range(n):
            for j in G1[i]:
                G2[perm[i]].append(perm[j])
        return G1, G2, perm
    else:
        G2 = erdos_renyi()
        return G1, G2, False

def lire_graphe():
    n, m = input(), input()
    adjacence = [[]]*n
    for i in range(n):
        nombre_voisins_i = input()
        for _ in range(i):
            adjacence[i].append(input())
    return n,adjacence
