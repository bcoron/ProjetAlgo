#!/usr/bin/env python
# -*- coding: utf-8 -*-

def equivalence_voisinage(n, v1, v2, bijection):
    if len(v1) != len(v2):
        return False

    for j in v1:
        #test limité aux n premiers sommets de G1
        if (j<n) and not(bijection[j] in v2):
            return False
    return True


# La bijection est un tableau t tel que t[i] = p(i) où p est une bijection.
# G1, G2 sont des graphes définis par liste d'adjacence.
# n : nombre de somets
def tester_isomorphisme(n, G1, G2, bijection):
    for i in range(n):
        voisins_p_i = G2[bijection[i]] # voisins de p(i) dans G2.
        voisins_i   = G1[i]
        if not(equivalence_voisinage(n, voisins_i, voisins_p_i, bijection)):
            return False
    return True

def choisir_image(n, G1, G2, position, choisi, bijection):
    for test in range(bijection[position]+1,n):
        if not(choisi[test]):
            voisins_position = G1[position]
            voisins_test     = G2[test]
            bijection[position] = test

            if equivalence_voisinage(position+1, voisins_position,
                                     voisins_test, bijection):
                return test
    return -1

def backtracking_glouton(n, G1, G2):
    bijection = [-1]*n
    choisi    = [False]*n
    position  = 0

    while(position < n):
        # on doit déterminer l'image de v_position.
        choix = choisir_image(n, G1, G2, position, choisi, bijection)
        if choix != -1:
            # on avance.
            choisi[choix] = True
            bijection[position] = choix
            position += 1
        else:
            # on recule.
            position -= 1
            bijection[choisi[bijection[position]]] = False
            if position == 0 and bijection[position] == n-1:
                return False

    return bijection

# ==============================================================================
# ==================== FONCTIONS UTILITAIRES ===================================
# ==============================================================================

def lire_graphe():
    n, m = input(), input()
    adjacence = [[]]*n
    for i in range(n):
        nombre_voisins_i = input()
        for _ in range(i):
            adjacence[i].append(input())
    return n,adjacence
